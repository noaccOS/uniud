;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname 2019-10-21.ex2) (read-case-sensitive #t) (teachpacks ((lib "drawings.ss" "installed-teachpacks"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "drawings.ss" "installed-teachpacks")) #f)))

(define (glue-tiles* tiles)
  (if (> (length tiles) 2)
      (glue-tiles (first tiles) (glue-tiles* (rest tiles)))
      (glue-tiles (first tiles) (second tiles))
      ))

(define croce
  (glue-tiles*
   (list
    larger-tile
    (shift-down (shift-right (half-turn larger-tile) 1.6) .8)
    (shift-right smaller-tile 1.6)
    (shift-right (shift-down (half-turn smaller-tile) 4) 1.6)
   )))

(define quadratino
  (glue-tiles*
   (list
        smaller-tile
        (half-turn (shift-down smaller-tile .8))
    )))

(define quadrato
  (glue-tiles*
   (list
    quadratino
        (shift-down quadratino .8)
    
    )
    ))

(define quadratone
  (glue-tiles*
   (list
    croce
    quadrato
    (shift-right quadrato 3.2)
    (shift-down quadrato 3.2)
    (shift-down (shift-right quadrato 3.2) 3.2)
    )
   ))

(define quadrato-inclinato
  (glue-tiles*
   (list
    (half-turn larger-tile)
    (shift-right(shift-down larger-tile 0.8) 1.6)
    (shift-right (half-turn smaller-tile) 1.6)
    (shift-right (shift-down smaller-tile 4) 1.6)
    )))

quadrato-inclinato
