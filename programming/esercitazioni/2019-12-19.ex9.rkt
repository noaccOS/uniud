#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname tmp) (read-case-sensitive #t) (teachpacks ((lib "hanoi.ss" "installed-teachpacks") (lib "drawings.ss" "installed-teachpacks"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "hanoi.ss" "installed-teachpacks") (lib "drawings.ss" "installed-teachpacks")) #f)))

(define alphabet '(#\A #\B #\C #\D #\E #\F #\G #\H #\I #\L #\M #\N #\O #\P #\Q #\R #\S #\T #\V #\X))

(define (index item l)
  (if (char=? item (first l))
      0
      (add1 (index item (rest l)))
      ))

(define (transform char k)
  (if (char=? char #\space)
      #\space
      (list-ref
       alphabet
       (remainder
        (+ (index char alphabet) k)
        (length alphabet)
        ))))

(define (cr k str)
  (if (string=? str "")
      str
      (string-append
       (string (transform (string-ref str 0) k))
       (cr k (substring str 1))
       )))

(define (caesar k)
  (lambda (str)
    (cr k str)
    ))

(define (s2 u v)
  (add1 v))

(define (h m n f g)
  (if (= n 0)
      (f m)
      (g m (h m (sub1 n) f g))
      ))

(define (H f g)
  (lambda (m n)
    (h m n f g)
  ))

(define add (H (lambda (x) x) s2 ))
(define mul (H (lambda (x) 0) add))
(define pow (H (lambda (x) 1) mul))
