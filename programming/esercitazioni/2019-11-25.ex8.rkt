#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname tmp) (read-case-sensitive #t) (teachpacks ((lib "hanoi.ss" "installed-teachpacks") (lib "drawings.ss" "installed-teachpacks"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "hanoi.ss" "installed-teachpacks") (lib "drawings.ss" "installed-teachpacks")) #f)))

(define hanoi-moves ; val: lista di coppie
  (lambda (n) ; n > 0 intero
    (hanoi-rec n 1 2 3)
    ))

(define hanoi-rec ; val: lista di coppie
  (lambda (n s d t) ; n intero, s, d, t: posizioni
    (if (= n 1)
        (list (list s d))
        (let ((m1 (hanoi-rec (- n 1) s t d))
              (m2 (hanoi-rec (- n 1) t d s))
              )
          (append m1 (cons (list s d) m2))
          ))
    ))

(define (hanoi-disks-rec moves k crossbars)
  (if (= k 0)
      crossbars
      (let* ((move (first moves)) (from (first move)) (to (second move)) (item (first (list-ref crossbars (sub1 from)))))
        (cond
          [(= from 1) (cond
                        [(= to 2) (hanoi-disks-rec (rest moves) (- k 1) (list (rest (first crossbars)) (cons item (second crossbars)) (third crossbars)))]
                        [(= to 3) (hanoi-disks-rec (rest moves) (- k 1) (list (rest (first crossbars)) (second crossbars) (cons item (third crossbars))))]
                        )]
          [(= from 2) (cond
                        [(= to 1) (hanoi-disks-rec (rest moves) (- k 1) (list (cons item (first crossbars)) (rest (second crossbars)) (third crossbars)))]
                        [(= to 3) (hanoi-disks-rec (rest moves) (- k 1) (list (first crossbars) (rest (second crossbars)) (cons item (third crossbars))))]
                        )]
          [(= from 3) (cond
                        [(= to 1) (hanoi-disks-rec (rest moves) (- k 1) (list (cons item (first crossbars)) (second crossbars) (rest (third crossbars))))]
                        [(= to 2) (hanoi-disks-rec (rest moves) (- k 1) (list (first crossbars) (cons item (second crossbars)) (rest (third crossbars))))]
                        )]
          ))))

(define (hanoi-comb n k)
  (hanoi-disks-rec (hanoi-moves n) k (list (range 1 (+ n 1) 1) (list) (list)))
  )

(define (hanoi-disks n k)
  (let ((crossbars (hanoi-comb n k)))

    (list
     (list 1 (length (list-ref crossbars 0)))
     (list 2 (length (list-ref crossbars 1)))
     (list 3 (length (list-ref crossbars 2)))
     )))

(define (aos pic background)
  (cond
    [(null? pic) background]
    [(null? background) pic]
    [else (above pic background)]
      ))

(define (inner-rec crossbar n p)
  (if (null? crossbar)
      null
      (aos
       (disk-image (first crossbar) n p (sub1 (length crossbar)))
       (inner-rec (rest crossbar) n p)
       )))

(define (outer-rec crossbars n)
  (if (null? crossbars)
      null
      (aos
       (inner-rec (first crossbars) n (length crossbars))
       (outer-rec (rest crossbars) n)
       )))

(define (hanoi-picture n k)
  (aos (outer-rec (reverse (hanoi-comb n k)) n) (towers-background n))
  )
