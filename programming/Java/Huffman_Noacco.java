import huffman_toolkit.OutputTextFile;
import java.util.Random;
import java.util.stream.IntStream;

public class Huffman_Noacco {
  String inputFile;
  int[] freq;
  Node root;
  String[] codes;

  private static String toLiteral(char c) {
    if (c == '\n')
      return "\\n";
    else if (c == '\t')
      return "\\t";
    else if (c == '\r')
      return "\\r";
    else if (c == ' ')
      return "SPC";
    else
      return "" + c;
  }

  public Huffman_Noacco(String inputFile) { this.inputFile = inputFile; }

  public void make_histogram() {
    freq = Huffman.charHistogram(inputFile);
    root = Huffman.huffmanTree(freq);
    codes = Huffman.huffmanCodesTable(root);
  }

  public void write_histogram(String outputFile) {
    OutputTextFile output = new OutputTextFile(outputFile);
    if (root == null)
      make_histogram();
    for (int i = 0; i < 128; i++) {
      if (freq[i] != 0) {
        output.writeTextLine(i + "\t" + toLiteral((char)i) + "\t" + freq[i] +
                             "\t" + codes[i] + "\t" + codes[i].length());
      }
    }
    output.close();
  }

  // Can be off by some bits
  public long estimate_size() {
    long compression_bits = IntStream.range(0, 128)
                                .filter(i -> freq[i] != 0)
                                .mapToLong(i -> freq[i] * codes[i].length())
                                .sum();

    long compression = (long)Math.ceil(compression_bits / 7);

    long header =
        String.valueOf(root.weight()).length() + // Ogni carattere pesa 1B
        Huffman.flattenTree(root).length() + 2;  // Ci sono due '\n'

    return compression + header;
  }

  public void write_compression(String outputFile) {
    Huffman.compress(inputFile, outputFile);
  }

  public void decompress(String from_file) {
    Huffman.decompress(from_file, this.inputFile);
  }

  public static void make_random_text_file(String outputFile, int length) {
    Random random = new Random();
    OutputTextFile out = new OutputTextFile(outputFile);
    IntStream.range(0, length).forEach(
        i -> out.writeChar((char)random.nextInt(128)));
    out.close();
  }
}
