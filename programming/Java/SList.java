public class SList<T> {
    private T first;
    private SList<T> rest;

    public SList() {
    }

    public SList(T e, SList<T> s1) {
        first = e;
        rest = s1;
    }

    public boolean isNull() {
        return first == null && (rest == null || rest.isNull());
    }

    public T car() {
        return first;
    }

    public SList<T> cdr() {
        return rest;
    }

    public SList<T> cons(T e) {
        if (first == null) {
            first = e;
            rest = new SList<T>();
        } else
            rest.cons(e);
        return this;
    }

    public int length() {
        if (first == null)
            return 0;
        else
            return 1 + (rest == null ? 0 : rest.length());
    }

    public T listRef(final int n) {
        return n == 0 ? first : rest.listRef(n - 1);
    }

    public boolean equals(final SList<T> s1) {
        return this.first == s1.first && this.rest == null ? s1.rest == null : this.rest.equals(s1.rest);
    }

    public SList<T> append(SList<T> s) {
        if (first == null) {
            first = s.first;
            rest = s.rest;
        } else {
            if (rest.isNull())
                rest = s;
            else
                rest.append(s);
        }
        return this;
    }

    public SList<T> reverse() {
        return isNull() ? this : rest.reverse().cons(first);
    }

    public boolean contains(T element) {
        return isNull() ?
            false :
            (car().equals(element) ?
                true :
                cdr().contains(element));
    }

    public String toString() {
        return "[" + toStringRec() + "]";
    }

    private String toStringRec() {
        return isNull() ? 
            "" : 
            first.toString() + 
            (rest.isNull() ? 
                "" : 
                ", " + rest.toStringRec());
    }

    public SList<T> remove(T s) {
        return first.equals(s) ? 
            rest : 
            new SList<T>(car(), cdr().remove(s));
    }
}