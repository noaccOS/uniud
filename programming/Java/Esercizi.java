import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.concurrent.CompletionException;

import puzzleboard.PuzzleBoard;

public class Esercizi {

    // Problema 10
    public static String next_btr(String btr) {
        int lastindex = btr.length() - 1;
        char lastchar = btr.charAt(lastindex);
        if (btr.length() == 1) {
            if (lastchar == '+')
                return "+-";
            else
                return "+";
        } else {
            String pre = btr.substring(0, lastindex);
            if (lastchar == '+')
                return next_btr(pre) + "-";
            else
                return pre + (lastchar == '-' ? "." : "+");
        }
    }

    public static String one_complement(String bits) {
        StringBuilder s = new StringBuilder(bits);
        for (int i = 0; i < s.length(); i++)
            s.setCharAt(i, s.charAt(i) == '1' ? '0' : '1');
        return s.toString();
    }

    public static StringSList n_consecutive_btrs(String btr, int n) {
        StringSList ssl = new StringSList();
        ssl.cons(btr);
        String current = btr;
        for (int i = 1; i < n; i++) {
            current = next_btr(current);
            ssl.cons(current);
        }
        return ssl;
    }

    // Project: LIS
    private static Integer[][] M;

    public static int llis(int[] arr) {
        M = new Integer[arr.length][arr.length];
        return llisRec(arr, 0, 0);
    }

    private static int llisRec(int[] arr, int i, int t) {
        if (M[i][t] != null)
            return M[i][t];
        if (i == arr.length)
            return 0;
        else if (arr[i] <= t)
            return M[i][t] = M[i + 1][t] = llisRec(arr, i + 1, t);
        else
            return M[i][t] = Math.max(1 + llisRec(arr, i + 1, arr[i]), llisRec(arr, i + 1, t));
    }

    public static int llisArrCount() {
        int count = 0;
        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[i].length; j++) {
                if (M[i][j] != null)
                    count++;
            }
        }
        return count;
    }

    private static IntSList[][] mem;

    public static IntSList lis(int[] arr) {
        mem = new IntSList[arr.length][arr.length];
        return lisRec(arr, 0, 0);
    }

    private static IntSList lisRec(int[] arr, int i, int t) {
        if (mem[i][t] != null)
            return mem[i][t];
        if (i == arr.length)
            return new IntSList();
        else if (arr[i] <= t)
            return mem[i][t] = mem[i + 1][t] = lisRec(arr, i + 1, t);
        else
            return mem[i][t] = lisRec(arr, i + 1, t).length() > lisRec(arr, i + 1, arr[i]).length()
                    ? lisRec(arr, i + 1, t)
                    : lisRec(arr, i + 1, arr[i]).cons(arr[i]);
    }

    // Problema 13
    public static void puzzle_game(int n) {
        PuzzleBoard gui = new PuzzleBoard(n);
        Puzzle15Board board = new Puzzle15Board(n);

        // init
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int num = board.at(i, j);
                if (num != 0)
                    gui.setNumber(i + 1, j + 1, num);
            }
        }
        gui.display();

        // gameplay
        while (!board.completed()) {
            int number = gui.get();
            int[] coords = board.find(number);
            int[] moved = board.move(coords[0], coords[1]);
            if (moved[0] != -1) {
                gui.setNumber(moved[0] + 1, moved[1] + 1, number);
                gui.clear(coords[0] + 1, coords[1] + 1);
                gui.display();
            }
        }

        gui.dispose();
    }

    public static void println(Object o) {
        System.out.println(o);
    }

    public static int llcs3(String t, String u, String v) {
        return llcs3Rec(t, u, v, new Integer[t.length() + 1][u.length() + 1][v.length() + 1]);
    }

    private static int llcs3Rec(String t, String u, String v, Integer[][][] mem) {
        if (mem[t.length()][u.length()][v.length()] != null)
            return mem[t.length()][u.length()][v.length()];
        if (t.equals("") || u.equals("") || v.equals(""))
            return mem[t.length()][u.length()][v.length()] = 0;
        else if (t.charAt(0) == u.charAt(0) && t.charAt(0) == v.charAt(0))
            return mem[t.length()][u.length()][v.length()] = 1 + llcs3(t.substring(1), u.substring(1), v.substring(1));
        else
            return mem[t.length()][u.length()][v.length()] = Math.max(
                    Math.max(llcs3(t.substring(1), u, v), llcs3(t, u.substring(1), v)),
                    Math.max(llcs3(t.substring(1), u, v), llcs3(t, u, v.substring(1))));
    }

    public static boolean symmetric(int[][] matrix) {
        for (int i = 1; i < matrix.length; i++) {
            for (int j = 0; j < i; j++) {
                if (matrix[i][j] != matrix[j][i])
                    return false;
            }
        }
        return true;
    }

    public static int shortestCodeLength( Node root ) {    
        int sc = 128;    
        Stack<Node> stack = new Stack<Node>();    
        Stack<Integer> depth = new Stack<Integer>();    
        stack.push( root );    
        depth.push( 0 );    
        do {        
            Node n = stack.pop();      
            int  d = depth.pop();      
            if ( n.isLeaf() ) {        
                sc = Math.min( sc, d );      
            } else if ( d+1 < sc ) {
                stack.push(n.left());
                stack.push(n.right());
                depth.push(d+1);
                depth.push(d+1);
            }    
        }  while (!stack.empty());
        return sc;  
    }
    
    public static double[] closestPair(double[] arr){
        double[] copy = arr.clone();
        Arrays.sort(copy);
        double[] minimum = new double[]{copy[0], copy[copy.length - 1]};
        for (int i = 0; i < copy.length - 2; i++){
            if(copy[i+1] - copy[i] < minimum[1] - minimum[0]){
                minimum[0] = copy[i];
                minimum[1] = copy[i+1];
            }
        }
        return minimum;
    }

    public static void main(String[] args) {
        System.out.println(symmetric(new int[][] { { 1, 2, 3 }, { 2, 4, 5 }, { 3, 5, 10 } }));
        System.out.println(symmetric(new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }));
    }
}
