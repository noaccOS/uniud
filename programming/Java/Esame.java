import java.util.Arrays;
import java.util.Stack;

public class Esame {

    public static void P2(int[] v){
        int n = v.length;

    if ( n > 2 ) {

        int[] x = new int[]{ v[n-2], v[n-1] };

        for ( int i=n-3; i>=0; i=i-1 ) {

            v[i+2] = v[i];

        }

        v[1] = x[1]; v[0] = x[0];

    }
    }

    public static void P2_rev(int[] v){
        if(v.length <= 2)
            return;
        int a = v[0];
        int b = v[1];
        for (int i = 0; i < v.length - 2; i++)
            v[i] = v[i+2];
        v[v.length - 1] = b;
        v[v.length - 2] = a;
    }

    static int xMin = 8, xMax = 8, yMin = 5, yMax = 5, zMin = 12, zMax = 12;

    public static long rec( int x, int y, int z ) {    // 1 <= x, y <= z

        if ( (x == 1) || (y == z) ) {

            return 1;

        } else {

            return rec( x-1, y, z ) + x * rec( x, y+1, z );

        }

    }

    public static long iter(int x, int y, int z){

        long[][] mem = new long[x+1][z+1];
        for(int i = 1; i <= x; i++){
            for(int j = z; j >= y; j--){
                if(i == 1 || j == z)
                    mem[i][j] = 1;
                else
                    mem[i][j] = mem[i-1][j] + i * mem[i][j+1];
            }
        }

        return mem[x][y];
    }

    public static String hanoi( Towers hts, int t ) { // hts: stato iniziale gioco, t: posizione finale torre

        hanoiRec( t, hts.height(), hts );

        return hts.moves();

    }


    private static void hanoiRec( int t, int n, Towers hts ) {

        if ( n != 0 ) {

            if ( n < 0 ) {

                hts.move( -n, t );

            } else if ( hts.site(n) == t ) {

                hanoiRec( t, n-1, hts );

            } else {

                int x = hts.transit( n, t );

                hanoiRec( x, n-1, hts );

                hanoiRec( t, -n, hts );

                hanoiRec( t, n-1, hts );

        }}

    }

    
    public static void println(Object o){
        System.out.println(o);
    }
    public static void main(String[] args){
        println(rec(8,5,12));
        println(iter(8,5,12));
    }
    
}