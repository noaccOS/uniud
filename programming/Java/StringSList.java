public class StringSList {
    private String first;
    private StringSList rest;

    public StringSList(){ }

    public StringSList(final String e, final StringSList s1){
        first = e;
        rest = s1;
    }

    public boolean isNull(){
        return first == null && 
            (rest == null || rest.isNull());
    }

    public String car(){
        return first;
    }

    public StringSList cdr(){
        return rest;
    }

    public StringSList cons(String e){
        if (first == null){
            first = e;
            rest = new StringSList();
        }
        else
            rest.cons(e);
        return this;
    }

    public int length(){
        if (first == null)
            return 0;
        else
            return 1 + (rest == null ? 0 : rest.length());
    }

    public String listRef(final int n){
        return n == 0 ?
            first :
            rest.listRef(n - 1);
    }

    public boolean equals(final StringSList s1){
        return this.first == s1.first && 
            this.rest == null ? 
                s1.rest == null :
                this.rest.equals(s1.rest);
    }

    public StringSList append(StringSList s){
        if (first == null){
            first = s.first;
            rest = s.rest;
        }
        else{
            if (rest.isNull())
                rest = s;
            else
                rest.append(s);
        }
        return this;
    }

    public StringSList reverse(){
        return isNull() ?
            this :
            rest.reverse().cons(first);
    }


    public String toString(){
        return "[" + toStringRec() + "]";
    }

    private String toStringRec(){
        return isNull() ?
            "" :
            first +
            (rest.isNull() ?
                "" :
                ", " + rest.toStringRec());
    }
}