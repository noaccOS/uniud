public class NodeQueue{
    // Not a circular queue, not needed for this kind 
    Node[] structure = new Node[260];
    int begin = 0;
    int lastindex = 0;

    public NodeQueue(){ }

    public int size(){
        return lastindex - begin;
    }

    public Node peek(){
        return structure[begin];
    }

    public Node poll(){
        return structure[begin++];
    }

    // Insertion sort, more than enough for this priority queue's use case
    private void add_rec(Node n, int index){
        if(structure[index - 1].compareTo(n) < 0)
            add_rec(n, index - 1);
        else
            structure[index] = n;
    }

    public void add(Node n){
        add_rec(n, lastindex);
    }
}