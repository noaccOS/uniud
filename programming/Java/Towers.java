import java.util.ArrayList;
import java.util.Stack;
import java.util.stream.IntStream;

public class Towers {
    
private int n;
private ArrayList<Stack<Integer>> sites = new ArrayList<Stack<Integer>>();
private String movesString = "";

public Towers(int n){
    this.n = n;
    IntStream.range(0, 3).forEach(i -> sites.add(new Stack<Integer>()));
    IntStream.range(0, n+1).forEach(i -> put(0, n - i));
}

public void put(int disk, int rod){
    sites.get(rod).push(disk);
}

public void move(int disk, int dst){
    int pos = site(disk);
    sites.get(dst).push(sites.get(pos).pop());
    movesString += " " + pos + ":" + dst;
}

public int height(){
    return n;
}

public int site(int disk){
    for(int i = 0; i < 3; i++){
        if(sites.get(i).peek() == disk)
            return i;
    }
    return -1;
}

public int transit(int disk, int dst){
    return 3 - site(disk) - dst;
}

public String moves(){
    return movesString;
}
}