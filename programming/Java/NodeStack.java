public class NodeStack {
    Node[] structure = new Node[260];
    int newindex = 0;
    public NodeStack() { }

    public boolean empty(){
        return newindex == 0;
    }

    public Node peek(){
        return structure[newindex - 1];
    }

    public Node pop(){
        return structure[--newindex];
    }

    public void push(Node n){
        structure[newindex] = n;
    }
}