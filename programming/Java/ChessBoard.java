public class ChessBoard {
  private int n;
  private SList<Integer> usedRows = new SList<Integer>();
  private SList<Integer> usedColumns = new SList<Integer>();
  private SList<Integer> usedPrimaryDiagonals = new SList<Integer>();
  private SList<Integer> usedSecondaryDiagonals = new SList<Integer>();
  private SList<String> queens = new SList<String>();

  public ChessBoard(int n) { this.n = n; }

  public int size() { return n; }

  public int queensOn() { return queens.length(); }

  public boolean underAttack(int i, int j) {
    return (usedRows.contains(i) || usedColumns.contains(j) ||
            usedPrimaryDiagonals.contains(i + j) ||
            usedSecondaryDiagonals.contains(i - j));
  }

  public static String coords(int i, int j) {
    char first = (char)((int)('a') + i - 1);
    int second = j;
    return "" + first + second;
  }

  public ChessBoard addQueen(int i, int j) {
    ChessBoard b = new ChessBoard(n);
    b.usedRows = new SList<Integer>(i, usedRows);
    b.usedColumns = new SList<Integer>(j, usedColumns);
    b.usedPrimaryDiagonals = new SList<Integer>(i + j, usedPrimaryDiagonals);
    b.usedSecondaryDiagonals =
        new SList<Integer>(i - j, usedSecondaryDiagonals);
    b.queens = new SList<String>(coords(i, j), queens);
    return b;
  }

  public String arrangement() { return arrangement_rec(queens); }

  private String arrangement_rec(SList<String> l) {
    return l.isNull()
        ? ""
        : l.car() + (l.cdr().isNull() ? "" : " " + arrangement_rec(l.cdr()));
  }

  public String toString() { return queens.toString(); }
}
