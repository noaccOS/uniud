II Periodo Didattico
====================


##  Lezione 32 - 31/03/20 

1. Astrazione sui dati:
   conta ispirata a un racconto di Giuseppe Flavio (I sec. d.C.; pretesto).

   - Descrizione del gioco: "conta" (vedi "Concrete Abstractions", sez. 3.5;
     Graham, Knuth & Patashnik, "Concrete Mathematics", 1989).

   - Il gioco e' una utile metafora degli "oggetti" della programmazione: stato
     del gioco (struttura dati); informazioni sullo stato e regole (operazioni).

   - Regole della versione rielaborata da Knuth: esce un cavaliere ogni due
     (vedi note associate all'esempio nelle pagine del corso).

   - Esempio con n = 6 cavalieri:

          A*            A             A
       F     B       F             F
                -->           -->
       E     C       E     C*      E*    C
          D             D

                 A*            A

          -->           -->           -->
              E     C       E*            E*
                                       
   - Da identificatori simbolici a identificatori numerici:

       gflavio(6) = 5  (E, quinto cavaliere);

   - Vedi applicazioncine nelle pagine del corso (note relative all'esempio).

   - Operazioni consentite:
     osservazioni/domande sullo stato e regole del gioco.

n2. Definizione del protocollo del dato astratto "tavola rotonda" (RoundTable),
   delineato in collaborazione con gli studenti:

     [ costruttore/i ]
       ...

     [ operazioni per acquisire informazioni su una particolare istanza ]
       ...

     [ operazioni per generare nuove istanze ]
       ...
     (oggetti immutabili!)

3. Soluzione del problema di Giuseppe Flavio,
   dato il protocollo definito sopra -- ma non ancora implementato!

   - Punto di vista del programmatore "utente" del dato astratto "tavola-rotonda".

4. Realizzazione del dato astratto "tavola rotonda".

   - Punto di vista dell' "implementatore" del dato astratto "tavola-rotonda".

   - Come rappresentare la disposizione dei cavalieri?

   - Come rappresentare il cavaliere con la brocca di sidro?

   - Realizzazione basata su una lista di cavalieri (IntSList)
     interpretata ciclicamente: il primo elemento della lista ha la brocca.

   - Implementazione delle operazioni previste dal protocollo:
     e' quanto da' significato al dato astratto (mantenimento della coerenza).

   - Visualizzazione attraverso diagrammi:

       (A B C D E F)  -*->  (C D E F A)  -*->  (E F A C)

         -*->  (A C E)  -*->  (E A)  -*->  (E)

   - Realizzazione dei metodi.

   - Ruolo del costruttore privato: le scelte relative alla rappresentazione
     interna non devono essere esposte all'esterno attraverso il protocollo!

5. Realizzazione alternativa del dato astratto "tavola rotonda".

   - Realizzazione basata su un array di cavalieri (int[]).

   - Modifiche relative alla realizzazione dei metodi.

6. Esercizi:
   - definire una classe StringSList per rappresentare liste di stringhe
     nello stile di Scheme;
   - sperimentazione: sequenza dei risultati al variare del numero
     di cavalieri n = 1, 2, 3, ...: suggerisce qualcosa?


Riferimenti: vedi anche introduzione alla II parte e sez. 6.1 del libro
consigliato; per il problema di Giuseppe Flavio vedi sez. 3.5.


##  Lezione 33 - 2/04/20 

1. Ancora una diversa realizzazione del dato astratto "tavola rotonda":
   versione "piu' efficiente" basata su una coppia di liste.

   1. Obiettivo: riduzione dei costi della ricostruzione di liste.

        - Principale causa di inefficienza: ricostruzione completa
          della lista (append) ad ogni passo del gioco.

   2. La lista della realizzazione precedente puo' essere spezzata in due:

        - I cavalieri della prima lista sono seguiti da quelli
          della seconda lista in ordine rovesciato;

        - La prima lista contiene sempre almeno un elemento
          (che identifica il cavaliere con la brocca);

        - Nella maggior parte dei casi e' sufficiente spostare
          un cavaliere dalla prima lista alla seconda;

        - Saltuariamente, circa ad ogni dimezzamento del numero di cavalieri,
          la prima lista deve essere ricostruita rovesciando la seconda.

2. Impostazione della una nuova rappresentazione.

   1. Variabili di istanza:

        - prima lista (IntSList),
          contenente il cavaliere con la brocca e parte degli altri;

        - seconda lista (IntSList),
          contenente i restanti cavalieri in ordine inverso;

        - numero complessivo di cavalieri (int).

   2. Visualizzazione (sempre riferita all'esempio della lezione precedente):

          ((A B C D E F) () 6)  -*->  ((C D E F) (A) 5)  -*->  ((E F) (C A) 4)

            -*->  [ (() (E C A) 3)  non ammessa come rappresentazione ]

            -*->  ((A C E) () 3)  ricostruzione

            -*->  ((E) (A) 2)  -*->  ((E) () 1)

3. Realizzazione della rappresentazione alternativa, piu' efficiente
   (vedi esempio nelle pagine del corso).

   - Realizzazione del costruttore  new RoundTable(int)

   - Realizzazione dei metodi  numberOfKnightsIn()  e  knightWithJugIn()

   - Realizzazione del metodo  afterNextKnightQuits()
     (analisi dei casi).

   - N.B. Il programma risolutivo non deve essere modificato!
     Quanto concordato sul protocollo e sulla relativa interpretazione
     resta ancora valido (e vincolante) per la nuova versione.

4. Stima dei costi computazionali per le realizzazioni discusse,
   basate su una lista oppure su un array di interi.

   1. Analisi dei costi computazionali:

        - Stime basate sul numero di operazioni chiave (cons, car, ..., v[i]=, ...)
          in un senso che verra' chiarito nel corso di Algoritmi e Strutture Dati;

        - Rilevanza delle scelte implementative.

   2. Numero di cons (oppure assegnazioni v[i]=) per aggiornare la lista/l'array
        in funzione del numero iniziale di cavalieri n:  C(n).

        - Numero di cons/v[i]= come misura rappresentativa dei costi computazionali:
          crescita del carico computazionale al crescere di n;

        - Nel seguito si farÃ  riferimento alla rappresentazione interna basata
          su una lista, contando il numero di operazioni "cons"...

        - ...se si contassero le assegnazioni a componenti dell'array
          il risultato sarebbe comunque identico.

   3. Analisi (caso della rappresentazione interna basata su una lista):

        - Quante volte viene invocata "cons" (range)
          per costruire la lista iniziale relativa a n cavalieri?

        - Quante volte viene invocata "cons" (append)
          per costruire una nuova lista di k elementi?

        - A partire da una lista di k+1 elementi, si ricostruisce
          (da capo) una lista di lunghezza k:  k "cons".

        - josephus(n) richiede n-1 passi della conta, durante i quali
          vengono ricostruite liste di n-1, n-2, ... 2, 1 elementi:

              C(n)  =  n  (costruzione della rappresentazione iniziale)

                       + n-1 + n-2 + ... + 2 + 1  (successive ricostruzioni)

              C(n)  =  n + n-1 + n-2 + ... + 2 + 1  =  n(n+1) / 2

   4. Essenzialmente: C(n) cresce in proporzione al quadrato di n,
                        circa n^2 * 1/2.

        - Esempi:  C(5) = 15;  C(10) = 55;  C(50) = 1275;  C(100) = 5050;
                   C(1000) > 500000.

5. Stima dei costi computazionali per la realizzazione piu' raffinata,
   basate su due liste di interi.

   1. Numero di "cons" per aggiornare le rappresentazioni in funzione
        del numero iniziale di cavalieri n:  C(n).

   2. Analisi.

        - n "cons" per costruire la rappresentazione iniziale
          (prima lista interna).

              I(n)  =  n

        - Almeno 1 "cons" ad ogni passo (dopo-uscita-commensale):

            IntSList v = follow.cons( knights.car() );

        - La valutazione di  (josephus n)  richiede n-1 passi:

              C'(n)  =  n-1

        - Ad ogni dimezzamento del numero di cavalieri viene rovesciata la
          lista "v" per ricostruisce la prima lista, di lunghezza (circa)
          n/2, n/4, n/8, ... :

              C"(n)  = [circa]  n/2 + n/4 + ... + n/(2^i) + ...

                     = [circa]  n (1/2 + 1/4 + ... + 1/(2^i) + ...)

                     = [circa]  n

        - Costi complessivi:

              C(n)  =  I(n) + C'(n) + C"(n)  = [circa]  3n

   3. Essenzialmente: C(n) cresce linearmente con n (circa 3n).

        - Esempi:  C(5) = 15;  C(10) = 30;  C(50) = 150;  C(100) = 300;
                   C(1000) = 3000  [circa].

6. Esercizio:
   - sperimentazione al fine di apprezzare la differenza nei tempi rilevati
     (ripetute invocazioni della procedura josephus);
   - verifica sperimentale che le diverse realizzazioni sono logicamente
     equivalenti (determinano gli stessi risultati);
   - verifica che il trend di crescita dei tempi rilevati in funzione
     del numero n di cavalieri con le diverse realizzazioni e'
     quadratico (tempo / n^2 = ...) oppure lineare (tempo / n = ...).


Riferimenti: vedi anche introduzione alla II parte e sez. 6.1 del libro
consigliato; per quanto riguarda le liste, vedi sezz. 7.1 e 2.

##  Lezione 41 - 7/05/20 

1. Codifica di Huffman per la compressione di documenti.

   1. Compressione dell'informazione su una sequenza di simboli.
        Esempio (sequenza di nucleotidi, 12 simboli):

            A T T C T A C C T T G T

   2. Codifica standard a parola fissa (2 bit per 4 simboli):

            A -> 00,  T -> 01,  C -> 10,  G -> 11

        - Codifica (24 bit):

            A T T C T A C C T T G T  ->  000101100100101001011101

   3. Codifica di Huffman:

        - Peso dei simboli = numero di occorrenze (o frequenza);
          coppie simbolo/peso ordinate per peso crescente:

            ( G:1   A:2   C:3   T:6 )

        - Costruzione dell'albero di Huffman;
          aggregazione e riordinamento dei due elementi di minor peso:

            (  /\ : 1+2=3   C:3   T:6 )
              G  A

            (   /\ : 3+3=6   T:6 )
               /\ C
              G  A

            (    /\  : 6+6=12 )
                /\ T
               /\ C
              G  A

        - Codice = percorso nell'albero per raggiungere il simbolo
          (0=sinistra, 1=destra):

            A -> 001,  T -> 1,  C -> 01,  G -> 000

        - Codifica (21 bit):

            A T T C T A C C T T G T  ->  001110110010101110001

        - Decodifica: interpreto la sequenza binaria come istruzioni
          per scendere attraverso l'albero (0/sinistra, 1/destra);
          quando arrivo a una foglia, riporto il simbolo corrispondente
          e ritorno dalla radice dell'albero.

2. Lavoro preparatorio:
   Strumenti per estendere l'accedesso ai file (di testo) al livello
   di singolo bit (oltre che di carattere o linea di testo).

   1. Canali per accedere ed operare su file di testo:

        - package huffman_toolkit

   2. Accesso in lettura a un file di testo ( huffman_toolkit.InputTextFile ):

          public static final int CHARS = 128
  
          public InputTextFile( String src )
          public boolean textAvailable()
          public boolean bitsAvailable()
          public String readTextLine()
          public char readChar()
          public String readCode( int k )
          public int readBit()
          public void close()

        (vedi documentazione nelle pagine degli esempi.)

   3. Accesso in scrittura a un file di testo ( huffman_toolkit.OutputTextFile ):

          public static final int CHARS = 128
  
          public OutputTextFile( String dst )
          public void writeTextLine( String txt )
          public void writeChar( char c )
          public void writeCode( String code )
          public void writeBit( int bit )
          public void close()

        (vedi documentazione nelle pagine degli esempi.)

   4. Eccezioni di I/O ( huffman_toolkit.TextFileException ).

   5. Le istanze di InputTextFile e OutputTextFile sono oggetti!

        - Come oggetti, sono accessibili solo attraverso il protocollo specifico;

        - Esempi: duplicazione di un file di testo attraverso

            [i]    src.readTextLine()  /  dst.writeTextLine(String)

            [ii]   src.readChar()      /  dst.writeChar(char)

            [iii]  src.readBit()       /  dst. writeBit(int)

            [iv]   src.readCode(7)     /  dst. writeCode(String)

          (...e conteggio delle iterazioni!)

        - Da terminale (Linux/MacOS):

            javac -classpath "huffman_toolkit.jar:." IOExamples.java
            java  -classpath "huffman_toolkit.jar:." IOExamples IOExamples.java copy.txt [...]

          (Windows: sostituire ':' con ';')

3. Esercizi:
   - ripercorrere i passi concettuali delle operazioni di codifica
     e decodifica della tecnica di Huffman su altri esempi;
   - sperimentare l'uso delle classi del package 'huffman_toolkit'
     per ricopiare un file tramite readCode(1) e writeCode(...).


Riferimenti: per i concetti di classe e oggetto vedi sezz. 3.2-3.3
del libro di Sedgewick & Wayne.


##  Lezione 42 - 12/05/20 

0. Esempio articolato di programma orientato agli oggetti:
   compressione e decompressione di documenti testuali.

   - Richiamo: Tecnica di Huffman;

   - Richiamo: Strumento per organizzare l'I/O.

1. Albero di Huffman.

   - Struttura: classe Node (oggetti immutabili);

   - Protocollo: due costruttori!

   - Definizione dello stato interno e realizzazione.

2. Costruzione dell'albero di Huffman.

   1. 'Istogramma' delle frequenze dei caratteri:

        - Generazione di un "istogramma" (array)
          del numero di occorrenze di ciascun carattere.

   2. Coda con priorita'.

        - PriorityQueue<Node>  [ java.util ]

        - Protocollo:  new PriorityQueue<Node>(),
                       size(),  add(Node),  poll(),  peek()

        - Approccio alla costruzione dell'albero di Huffman
          a partire dall'istogramma delle frequenze dei caratteri;

        - Node:  implements Comparable<Node>  /  comareTo(Node)

        - Algoritmo che applica la coda con priorita'
          per costruire l'albero di Huffman.

   3. Costruzione della tabella dei codici di Huffman dei
        caratteri contenuti nel documento:

        - Visita ricorsiva dell'albero di Huffman;

        - Parametro: codifica binaria del percorso dalla radice
          dell'albero di Huffman al nodo correntemente visitato;

        - Parametro di stato: tabella in corso di compilazione;

        - Inizializzazioni per preparare la visita ricorsiva.

3. Esercizi:
   - revisione e sperimentazione del codice sviluppato.


Riferimenti: classi e oggetti sono trattati nelle sezz. 3.2-3.3 di
Sedgewick & Wayne; la sez. 3.4 presenta un esempio piu' ampio.


##  Lezione 43 - 14/05/20 

1. Compressione di documenti testuali con la tecnica di Huffman.

   1. Struttura del documento compresso.

        - Intestazione: numero caratteri + codifica testuale dell'albero;

        - Codifica di Huffman: trasposizione del contenuto del documento.

   2. Rappresentazione testuale "linearizzata" dell'albero di Huffman.

        - Codifica dei nodi foglia (carattere corrispondente)
          e dei nodi intermedi ('@');

        - Visita ricorsiva dell'albero e generazione della stringa
          che lo rappresenta;

        - Trattamento dei caratteri speciali ('@') e di controllo ('\').

   3. Compressione di un documento.

        - Scansione I:  istogramma delle frequenze;

        - Costruzione della tabella dei codici dei caratteri;

        - Informazioni per l'intestazione: dimensione del documento
          e codifica testuale dell'albero di Huffman;

        - Scansione II: codifica di Huffman;

        - Criteri di scrittura di bit e sequenze di bit.

   4. Verifiche sperimentali.

        - Compressione di un documento compresso!

        - Cosa accade?

2. Esercizi:
   - revisione e sperimentazione del codice sviluppato;
   - compressione di un documento di 5000 caratteri generato
     casualmente (distribuzione uniforme dei codici ASCII 0-127).


Riferimenti: classi e oggetti sono trattati nelle sezz. 3.2-3.3 di
Sedgewick & Wayne; la sez. 3.4 presenta un esempio piu' ampio.


##  Lezione 44 - 19/05/20 

1. Decompressione e ripristino di documenti testuali
   (vedi codice nelle pagine degli esempi).

   0. Comprimere va benissimo, ma a patto che siamo in grado
        di ripristinare il documento originale!

   1. Dispositivo per la decompressione.

        - Ripristino dell'albero di Huffman (schema ricorsivo);

        - Verifica di consistenza: stringa --> albero --> stringa.

        - Meccanismo di decodifica dei caratteri:
          percorsi attraverso l'albero di Huffman
          guidati dall'acquisizione di singoli bit.

   2. Decodifica e ripristino del documento.

   3. Verifiche sperimentali.

   4. Esempio: compressione di un documento compresso:

        - ripristino in due passi del documento originale;

        - fattore di compressione?

2. Ricorsione e iterazione.

   1. Trasposizione iterativa di un programma ricorsivo
        utilizzando uno stack.

   2. Esempio: rielaborazione iterativa del metodo statico "flattenTree":

        - simulazione manuale della soluzione basata sullo stack;

        - codifica: Stack<Node> .

   3. Esempio: rielaborazione iterativa del metodo statico "huffmanCodesTable":

        - la tabella dei codici e' di fatto una variabile di stato;

        - le ricorsioni dipendono pero' da due parametri: classe Frame;

        - codifica tramite Stack<Frame>.

3. Esercizi:
   - sperimentazione del funzionamento di PriorityQueue<Integer>
     ("compareTo" e' gia' definito per Integer) e Stack<Integer>;
   - verifica sperimentale su un documento prefissato che le versioni
     ricorsiva e iterativa con stack di "flattenTree" sono intercambiabili
     (intestazione e contenuto del file compresso identici);
   - realizzazione di classi "NodeQueue" e "NodeStack" che offrano
     funzionalita' equivalenti a PriorityQueue<Node> e Stack<Node>;
   - versione iterativa con lo stack del metodo statico "restoreTree
     (la trasformazione non e' immediata!).


Riferimenti: classi e oggetti sono trattati nelle sezz. 3.2-3.3 di
Sedgewick & Wayne; la sez. 3.4 presenta un esempio piu' ampio.


##  Lezione 45 - 21/05/20 

1. Verifica formale della correttezza dei programmi iterativi: preambolo.
   
   1. Moltiplicazione Egizia e moltiplicazione "del contadino Russo".

   2. Digressione storica (vedi appunti nella pagina degli esempi):

        - Moltiplicazione Egizia (Papiro di Rhind, circa 1650 a.C.);

        - Relazioni con l'algoritmo del contadino Russo (analisi di esempi);

        - Iterazione e ricorsione di coda consentono di fare i conti a mente
          (passo dopo passo, basta ricordare tre valori)...

        - ...Ma non la ricorsione generale o l'algoritmo originale degli Egizi
          (occorrerebbe tenere traccia di tutto il processo computazionale);

        - Perche' funziona? concetto di invariante!

  3. Concetto di "invariante".

        - Invarianti nell'analisi dei programmi iterativi;

        - Invarianti come strumento di analisi dei problemi in generale;

        - Invarianti nelle discipline scientifiche (geometria, fisica, chimica).

2. Verifica formale della correttezza "parziale".

   1. Formalizzazione delle specifiche.

        - Precondizioni: assunzioni sui dati di input;

        - Postcondizioni: relazioni fra risultati e dati di input.

   2. Formalizzazione della logica algoritmica: Asserzioni.

        - Invarianti di un comando iterativo
          (in corrispondenza alla condizione del ciclo -- 'guardia');

        - Proprieta' che si conservano ad ogni passo del ciclo.

   3. Correttezza parziale di un programma iterativo: Passi della verifica.

        - L'invariante vale all'inizio dell'iterazione;

        - L'invariante si conserva al generico passo iterativo;

        - Alla fine l'invariante permette di dedurre la postcondizione
          (l'asserzione che esprime le proprieta' del risultato atteso);

        - Passi della dimostrazione.

3. Verifica della correttezza "completa": terminazione.

   1. Funzione di terminazione di un comando iterativo
        --- intuitivamente: sovrastima del numero residuo di iterazioni;

   2. Proprieta' delle funzioni di terminazione:

        - Immagine discreta (numeri naturali);

        - Limite inferiore prefissato (zero);

        - Decresce strettamente ad ogni passo iterativo
          (quando vale l'invariante!).

   3. Funzione di terminazione per l'esempio considerato:

        - Scelta della funzione di terminazione;

        - Dimostrazione delle proprieta': verifica che la funzione
          introdotta e' effettivamente una funzione di terminazione...

        - ... purche' valga l'invariante!


Riferimenti: vedi appunti disponibili attraverso le pagine on-line
del corso su moltiplicazione Egizia e algoritmo del contadino russo;
vedi inoltre i costrutti per l'iterazione nella sez. 1.3 del libro
di Sedgewick & Wayne; per gli array vedi sez. 4.


##  Lezione 46 - 26/05/20 

1. Esempio di verifica formale della correttezza:
   Calcolo del quadrato come somma di numeri dispari consecutivi
   (versione imperativa di "unknown", lezione 17).

       // Elevamento al quadrato attraverso somme
  
       public static int sqr( int n ) {  //  Pre:   n >= 0
       
         int x = 0;
         int y = 0;
         int z = 1;
         
         while ( x < n ) {  //  Inv:   0 <= x <= n,  y = x^2,  z = 2x + 1
                            //  Term:  n - x
           x = x + 1;
           y = y + z;
           z = z + 2;
         }
         return y;                       //  Post:  y = n^2
       }

   1. Formalizzazione delle specifiche.

        - Precondizioni - assunzioni sui dati di input:

            Pre:   n >= 0

        - Postcondizioni - relazioni fra risultati e dati di input.

            Post:  y = n^2

   2. Formalizzazione della logica algoritmica: Asserzioni.

        - Invarianti di un comando iterativo (in corrispondenza alla 'guardia'):
          proprieta' che si conservano ad ogni passo del ciclo;

            Inv(x,y,z) :   0 <= x <= n,  y = x^2,  z = 2x + 1

        - Significato dei simboli nelle asserzioni e nel programma:
          un'asserzione "parla" dei valori delle variabili.

   3. Correttezza parziale di un programma iterativo: Passi della verifica.

        - L'invariante vale all'inizio dell'iterazione:

            Inv(0,0,1) :   0 <= 0 <= n,  0 = 0^2,  1 = 20 + 1

                           [ n >= 0 segue da Pre ]

        - L'invariante si conserva al generico passo iterativo;

            Inv(x,y,z) :   (a) 0 <= x <= n,  (b) y = x^2,  (c) z = 2x + 1

            Guardia :      (d) x < n  (condizione del while)

              x = x + 1;
              y = y + z;
              z = z + 2;

            Inv(x+1,y+z,z+2) :   0 <= x+1 <= n,  y+z = (x+1)^2,
                                 z+2 = 2(x+1) + 1

        - Infatti:

            (a), (d)  ==>  0 <= x+1 <= n

            (b), (c)  ==>  y+z = x^2 + 2x+1 = (x+1)^2

            (c)       ==>  z+2 = 2x+1 + 2 = 2(x+1) + 1

        - Alla fine l'invariante implica la postcondizione:

            Inv(x,y,z),  (e) x >= n  (uscita dal while)

        - Quindi:

            (a), (e)  ==>  (f) x = n

            (b), (f)  ==>  y = x^2 = n^2  [ Post ]

   4.  Funzione di terminazione e correttezza completa.

        - Scelta della funzione di terminazione:

            Term(x,y,z) = n - x

        - Dimostrazione delle proprieta'...

        - A valori naturali:

            Inv(x,y,z)  ==>  Term(x,y,z) >= 0  [ per (a) ]

        - Decresce strettamente dopo un passo iterativo:

            Inv(x,y,z) :   (a) 0 <= x <= n,  (b) y = x^2,  (c) z = 2x + 1

            Guardia :      (d) x < n  (condizione del while)

              x = x + 1;
              y = y + z;
              z = z + 2;

            Term(x+1,y+z,z+2) = n - (x+1) = n-x - 1 = Term(x,y,z) - 1

2. Esempio di verifica formale della correttezza:
   Calcolo del minimo coumune multiplo di due interi positivi:

     public static int mcm( int m, int n ) {  // Pre:  m, n > 0
     
       int x = m;
       int y = n;
       
       while ( x != y ) {  // Inv:  0 < x, y <= mcm(m,n),  x mod m = y mod n = 0
       
         if ( x < y ) {
           x = x + m;
         } else {
           y = y + n;
         }
       }
       return x;  // Post:  x = mcm(m,n)
     }

   1. Correttezza parziale:

        - Impostazione analoga al caso precedente;

        - Trattamento dell' "if" annidato nel while: dimostrazione
          della conservazione dell'invariante articolata in due rami;

        - Invariante in corrispondenza alla condizione dell'iterazione;

        - L'invariante vale all'inizio dell'iterazione:

            0 < m, n <= mcm(m,n),  m mod m = n mod n = 0

        - L'invariante si conserva al generico passo iterativo:

             (i)  0 < x+m, y <= mcm(m,n),  (x+m) mod m = y mod n = 0

                  (assumendo Inv & x < y)

            (ii)  0 < x, y+n <= mcm(m,n),  x mod m = (y+n) mod n = 0

                  (assumendo Inv & x > y)

        - Alla fine l'invariante implica la correttezza del risultato:

            x mod m = x mod n = 0  (poiche' x = y)  ==>  x >= mcm(m,n)

            inoltre  x <= mcm(m,n)  per Inv

   2. Terminazione:

        - Scelta della funzione di terminazione:

            term(x,y,m,n) = 2 mcm(m,n) - x - y

        - Dimostrazione delle proprieta' della funzione di terminazione:

            term(x,y,m,n) >= 0  poiche' x, y <= mcm(m,n)

            term(x',y',m,n) <= term(x,y,m,n) - min(m,n) < term(x,y,m,n)

3. Esercizio:
- completare la dimostrazione relativa al ramo "else" nell'iterazione
  del programma per il calcolo del minimo comune multiplo.


Riferimenti: vedi i costrutti per l'iterazione nella sez. 1.3 del libro
di Sedgewick & Wayne; per gli array vedi sez. 4.


##  Lezione 47 - 28/05/20 

1. Esercizio:
   - dimostrazione di correttezza del seguente programma:
  
       // Cubo attraverso somme
  
       public static int cube( int n ) {  //  Pre:  n >= 0
       
         int x = 0;
         int y = 0;
         int u = 1;
         int v = 6;
         
         while ( x < n ) {  //  Inv:  0 <= x <= n,  y = x^3,
                            //        u = 3x^2 + 3x + 1,  v = 6x + 6
                            //  Term: ?
           x = x + 1;
           y = y + u;
           u = u + v;
           v = v + 6;
         }
         return y;  //  Post:  y = n^3
       }

   - programma per simulare la tecnica di moltiplicazione Egizia.

2. Esempio di verifica formale della correttezza:
   Fattorizzazione in fattori primi (utilizzando un array):

     public static int[] fattorizzazione( int n ) {  // Pre:  n >= 2

       int[] fattori = new int[ n+1 ];

       for ( int i=0; i<=n; i=i+1 ) {
         fattori[i] = 0;
       }
       int x = n;
       int p = 2;

       while ( x > 1 ) {      // Inv:  1 <= x <= n,
                              //       n = x * Prod (k: [2,n]) k^fattori[k],
                              //       x non ha fattori < p,  2 <= p <= n                 
                              // term: x + n - p
         if ( x % p == 0 ) {
           fattori[p] = fattori[p] + 1;
           x = x / p;
         } else {
           p = p + 1;
       }}
       return fattori;  // Post: n = Prod (k: [2,n]) k^fattori[k]
     }

   1. Correttezza parziale:

        - L'invariante vale all'inizio dell'iterazione:

            1 <= n <= n,  n = n * Prod (k: [2,n]) k^0,
            n non ha fattori < 2,  2 <= 2 <= n

        - L'invariante si conserva al generico passo iterativo:

             (i)  1 <= x/p <= n,
                  n = x/p * Prod (k: [2,p-1]) k^fattori[k] *
                      p^(fattori[k]+1) * Prod (k: [p+1,n]) k^fattori[k],
                  x/p non ha fattori < p,  2 <= p <= n

                  (assumendo Inv & x mod p = 0)

            (ii)  1 <= x <= n,
                  n = x * Prod (k: [2,n]) k^fattori[k],
                  x non ha fattori < p+1,  2 <= p+1 <= n

                  (assumendo Inv & x mod p > 0)

        - Alla fine Inv & x <= 1 implica la correttezza del risultato:

            1 <= x & x <= 1  implica  x = 1,  da cui

            n = 1 * Prod (k: [2,n]) k^fattori[k]

        - Introducendo qualche proprita' aggiuntiva si potrebbe
          facilmente dimostrare che i fattori (con esponente > 0)
          sono tutti numeri primi.

   2. Dimostrazione delle proprieta' della funzione di terminazione
        (in considerazione del fatto che vale l'invariante):

          x + n - p > 0

           (i)  x/p + n - p < x + n - p

          (ii)  x + n - (p+1) < x + n - p

3. Esercizio:
   - "completare" la postcondizione e l'invariante per dimostrare
     che i fattori con esponente maggiore di zero sono numeri primi.

4. Esempi di svolgimento di temi d'esame:
   - II prova di accertamento del 25/06/2019: es. 4/A (correttezza).

5. Esercizi:
   - temi d'esame in generale!


Riferimenti: vedi i costrutti per l'iterazione nella sez. 1.3 del libro
di Sedgewick & Wayne; per gli array vedi sez. 4.


##  Lezione 48 - 4/06/20 

1. Esempi di svolgimento di temi d'esame:
   - II prova di accertamento del 25/06/2019: ess. 2/A, 3/B.

2. Esempi di svolgimento di temi d'esame:
   - II prova di accertamento del 11/06/2018: ess. 3, 4, 5.

3. Esercizi:
   - versioni non svolte degli esercizi
     della prova di accertamento del 25/06/2019;
   - temi d'esame in generale!


##  Lezione 49 - 9/06/20 

1. Esempi di svolgimento di temi d'esame:
   - prova scritta del 11/07/2017: es. 5;
   - prova scritta del 23/07/2018: ess. 2, 5;

2. Esercizi:
   - prova scritta del 24/09/2019: es. 3.
   - prova scritta del 3/09/2019: es. 3.
   - temi d'esame in generale!


##  Lezione 50 - 11/06/20 

1. Discussione di temi d'esame proposti dagli studenti:
   - II prova di accertamento del 18/06/2015: ess. 1, 3;
   - prova scritta del 4/07/2016: es. 3.

2. Esercizi:
   - temi d'esame in generale!


##  Fine del corso 


