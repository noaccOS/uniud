public class Esercitazione{
    public static String one_complement(String bits) {
        StringBuilder s = new StringBuilder(bits);
        for (int i = 0; i < s.length(); i++)
            s.setCharAt(i, s.charAt(i) == '1' ? '0' : '1');
        return s.toString();
    }

    public static void main(String[] args){
        System.out.println(one_complement("1010101110"));
    }
}