public class LIS{

    // Same as List.stream().max()
    private static int max_value(int[] arr){
        int max = arr[0];
        for (int i : arr)
            if (max < i)
                max = i;
        return max;
    }

    private static Integer[][] M;

    public static int llis(int[] arr) {
        M = new Integer[arr.length + 1][max_value(arr) + 1];
        return llisRec(arr, 0, 0);
    }

    private static int llisRec(int[] arr, int i, int t) {
        if (M[i][t] != null)
            return M[i][t];
        if (i == arr.length)
            return 0;
        else if (arr[i] <= t)
            return M[i][t] = M[i + 1][t] = llisRec(arr, i + 1, t);
        else
            return M[i][t] = Math.max(1 + llisRec(arr, i + 1, arr[i]),
                                      llisRec(arr, i + 1, t));
    }

    public static int llisArrCount() {
        int count = 0;
        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[i].length; j++) {
                if (M[i][j] != null)
                    count++;
            }
        }
        return count;
    }

    private static IntSList[][] mem;

    public static IntSList lis(int[] arr) {
        mem = new IntSList[arr.length + 1][max_value(arr) + 1];
        return lisRec(arr, 0, 0);
    }

    private static IntSList lisRec(int[] arr, int i, int t) {
        if (mem[i][t] != null)
            return mem[i][t];
        if (i == arr.length)
            return new IntSList();
        else if (arr[i] <= t)
            return mem[i][t] = mem[i + 1][t] = lisRec(arr, i + 1, t);
        else
            return mem[i][t] = lisRec(arr, i + 1, t).length() >
                lisRec(arr, i + 1, arr[i]).length()
                ? lisRec(arr, i + 1, t)
                : lisRec(arr, i + 1, arr[i]).cons(arr[i]);
    }

    public static void main(String[] args){
        int[] arr = new int[] {9, 46, 54, 71, 60, 47, 0, 32, 25, 61};
        System.out.println(llis(arr));
        System.out.println(lis(arr));
        System.out.println(llisArrCount());
    }
}