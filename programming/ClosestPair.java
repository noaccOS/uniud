import java.util.Arrays;
public class ClosestPair{
    public static double[] closestPair(double[] arr){
        double[] copy = arr.clone();
        Arrays.sort(copy);
        double[] minimum = new double[]{copy[0], copy[copy.length - 1]};
        for (int i = 0; i < copy.length - 2; i++){
            if(copy[i+1] - copy[i] < minimum[1] - minimum[0]){
                minimum[0] = copy[i];
                minimum[1] = copy[i+1];
            }
        }
        return minimum;
    }

    public static void main(String[] args){
        System.out.println(Arrays.toString(closestPair(new double[] {0.3, 0.1, 0.6, 0.8, 0.5, 1.1})));
    }

}