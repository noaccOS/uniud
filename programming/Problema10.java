public class Problema10 {
    public static String btr_succ(String btr) {
        int lastindex = btr.length() - 1;
        char lastchar = btr.charAt(lastindex);
        if (btr.length() == 1) {
            if (lastchar == '+')
                return "+-";
            else
                return "+";
        } else {
            String pre = btr.substring(0, lastindex);
            if (lastchar == '+')
                return btr_succ(pre) + "-";
            else
                return pre + (lastchar == '-' ? "." : "+");
        }
    }

    public static StringSList n_consecutive_btrs(String btr, int n) {
        StringSList ssl = new StringSList();
        ssl.cons(btr);
        String current = btr;
        for (int i = 1; i < n; i++) {
            current = btr_succ(current);
            ssl.cons(current);
        }
        return ssl;
    }

    public static void main(String[] args){
        System.out.println(n_consecutive_btrs("+.+", 5));
    }
}