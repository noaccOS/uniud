(define (llcs s t)
  (if (or (= (string-length s) 0) (= (string-length t) 0))
      0
      (if (char=? (string-ref s 0) (string-ref t 0))
          (+ 1 (llcs (substring s 1) (substring t 1)))
          (max
           (llcs (substring s 1) t)
           (llcs s (substring t 1))
           ))))

(llcs "abc" "abc")
