import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class Puzzle15Board {
    private int n;              // Dimension of the board
    private int[] numbers;      // Cells
    private int[] modifiers;    // Used when moving pieces around

    public Puzzle15Board(int n){
        this.n = n;

        numbers = IntStream.range(0, n * n).toArray();
        this.shuffle_board();

        modifiers = new int[]{1, -1, n, -n};
    }

    private void shuffle_board(){
        Random random = new Random();
        for (int i = 0; i < numbers.length; i++){
            int j = random.nextInt(numbers.length);
            swap(i, j);
        }
    }

    public boolean completed(){
        return IntStream
            .range(0, numbers.length - 1)
            .allMatch(i -> numbers[i] == i + 1);
    }

    private boolean secure_check(int start, int end){
        return
            0 <= end     &&
            end < n * n  &&
            !((start / n != end / n) && (start % n != end % n)) &&
            numbers[end] == 0;
    }

    public boolean can_move(int x, int y){
        int index = x * n + y;
        return
            secure_check(index, index + 1) ||
            secure_check(index, index - 1) ||
            secure_check(index, index + n) ||
            secure_check(index, index - n);
    }

    private void swap(int start, int end){
        int tmp = numbers[start];
        numbers[start] = numbers[end];
        numbers[end] = tmp;
    }

    public int[] move(int x, int y){
        int start = x * n + y;
        for (int mod = 0; mod < modifiers.length; mod++){
            int end = start + modifiers[mod];
            if(secure_check(start, end)){
                swap(start, end);
                return new int[]{end / n, end % n};
            }
        }
        return new int[]{-1, -1};
    }

    public int at(int x, int y){
        return numbers[x * n + y];
    }

    public int[] find(int number){
        for (int i = 0; i < numbers.length; i++)
            if (numbers[i] == number)
                return new int[]{i / n, i % n};
        return new int[]{-1 , -1};
    }

    public String configuration(){
        return this.toString();
    }

    public String toString(){
        return Arrays.toString(numbers);
    }
}