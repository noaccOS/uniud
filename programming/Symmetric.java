public class Symmetric{
    public static boolean symmetric(int[][] matrix){
        for(int i = 1; i < matrix.length; i++){
            for(int j = 0; j < i; j++){
                if(matrix[i][j] != matrix[j][i])
                    return false;
            }
        }
        return true;
    }

    public static void main(String[] args){
        System.out.println(symmetric(new int[][]{{1, 2, 3}, {2, 4, 5}, {3, 5, 10}}));
        System.out.println(symmetric(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9} }));
    }
}