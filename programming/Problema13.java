import puzzleboard.PuzzleBoard;

public class Problema13{
    public static void puzzle_game(int n) {
      PuzzleBoard gui = new PuzzleBoard(n);
      Puzzle15Board board = new Puzzle15Board(n);

      // init
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          int num = board.at(i, j);
          if (num != 0)
            gui.setNumber(i + 1, j + 1, num);
        }
      }
      gui.display();

      // gameplay
      while (!board.completed()) {
        int number = gui.get();
        int[] coords = board.find(number);
        int[] moved = board.move(coords[0], coords[1]);
        if (moved[0] != -1) {
          gui.setNumber(moved[0] + 1, moved[1] + 1, number);
          gui.clear(coords[0] + 1, coords[1] + 1);
          gui.display();
        }
      }

      gui.dispose();
    }

    public static void main(String[] args){
        puzzle_game(4);
    }
}