public class Configurations{

    public static ChessboardView gui;

    // Copia incolla di numberOfCompletions cambiando i return
    // con chiamate di funzione o visualizzazione
    public static void viewCompletions ( ChessBoard b ) {

        int n = b.size();
        int q = b.queensOn();

        if ( q == n ) {

            gui.setQueens(b.arrangement());;

        } else {

            int i = q + 1;

            for ( int j=1; j<=n; j=j+1 ) {
                if ( !b.underAttack(i,j) ) {

                    viewCompletions( b.addQueen(i,j) );
                }}
        }
    }


    public static void main( String args[] ) {
        int n = 8;
        gui = new ChessboardView(n);
        completions(new ChessBoard(n));

    }
}