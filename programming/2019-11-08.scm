(define (manhattan x y)
  (if (or (= x 0) (= y 0))
      1  ;; Un modo per andare da un punto a un altro in una linea
      (+
       (manhattan x (- y 1))
       (manhattan (- x 1) y)
       )
      ))

(manhattan 3 3)
