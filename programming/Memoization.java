public class Memoization{

    public static void println(Object o) {
        System.out.println(o);
    }

    public static int llcs3(String t, String u, String v){
        return llcs3Rec(t, u, v, new Integer[t.length() + 1][u.length() + 1][v.length() + 1]);
    }

    private static int llcs3Rec(String t, String u, String v, Integer[][][] mem){
        if (mem[t.length()][u.length()][v.length()] != null)
            return mem[t.length()][u.length()][v.length()];
        if(t.equals("") || u.equals("") || v.equals(""))
            return mem[t.length()][u.length()][v.length()] = 0;
        else if(t.charAt(0) == u.charAt(0) && t.charAt(0) == v.charAt(0))
            return mem[t.length()][u.length()][v.length()] = 1 + llcs3(t.substring(1), u.substring(1), v.substring(1));
        else
            return mem[t.length()][u.length()][v.length()] = Math.max(
                                                                      Math.max(llcs3(t.substring(1), u, v), llcs3(t, u.substring(1), v)),
                                                                      Math.max(llcs3(t.substring(1), u, v), llcs3(t, u, v.substring(1)))
                                                                      );
    }

    public static void main(String[] args){
        System.out.println(llcs3("pino", "piano", "pane"));
    }
}