# Assembly cheatsheet

## Operaziomi matematiche

## Addizione
```add r0, r2, r3```  
```add r0, #7, #0xA```  
  
$r0 = r2+r3$  
$r0 = 7_{dec} + A_{hex}$

### Addizione con carry
``adc r0, r2, r3``

$r0 = r2 + r3 + c$  
Dove c è il riporto dell'operazione precendente

> Permette di fare somme a 64 bit

## Sottrazione
``sub r0, r1, r2``  
``rsb r0, r1, r2``  

$r0 = r1 - r2$  
$r0 = r2 - r1$

> Non accettano costanti come primo argomento, se devo fare ad esempio
> $r0 = -r2$ diventa
> ``rsb r0, r2, #0``

> rsb inverte gli operatori

## Moltiplicazione
``mul r0, r2, r3``  
$r0 = r2 \times r3$

> mul non ammette argomenti costanti, tipo ``mul r0, r1, #4``

## Operazioni logiche

### And
``and r0, r1, r2``

### Or
``orr r0, r1, r2``

### Xor
``eor r0, r1, r2``

### Bit clear
``bic r0, r1, r2``

$r0 = r2 \land \lnot r2$

### Move
``mov r0, r2``

### Move negate
``mvn r0, r3``

$r0 = \overline{r3}$
> Viene utilizzato anche per il not, facendo ``mvn r1, r1``

## Operazioni sui bit

### Bit shifting

- lsl, logical shift left  
  ``add r1, r2, r1, lsl #4``  
  ``1110 << 1`` $\to$ ``1100``
- lsr, logical shift right  
  ``1110 >> 1`` $\to$ ``0111``
- asr, arithmetic shift right  
  Usa il bit del segno anziché zeri  
  ``1110 >> 1`` $\to$ ``1111``  
  ``0110 >> 1`` $\to$ ``0011``  
- ror, rotate right  
  Inserisce i bit tolti anziché zeri  
  ``011101 >> 2`` $\to$ ``010111``  
- rrx, rotate right extended  
  - Come rotate right, ma usa anche il carry  
  - Last bit $\to$ New Carry  
  - Old Carry $\to$ First bit  
  - Old first $n-1$ bits $\to$ New last $n-1$ bits
  - ![RRX](imgs/rrx.jpg)

### Trasferimento dalla memoria
Copia 4 byte a partire da [posizione]  
``ldr r3, [posizione]``

posizione può essere
- Un registro ``[r0]``
- Una somma ``[r0, #8]``
- Somma shiftata ``[r0, r1, lsl r2]``
- Somma di cui viene mantenuto l'incremento (preincremento)
  - Esempio: ``[r0, #8]!``, Dopo l'operazione il registro ``r0`` rimarrà incrementato di 8
- Postincremento ``[r0], #8``
  - Come preincremento, ma ``r3`` contiene il valore di ``[r0]`` e ques'ultimo viene incrementato dopo

Inserimento in memoria: ``str`` anziché ``ldr``

## Salti

``cd`` specifica la condizione  
``b(cd) label``

### Condizioni
- ``eq`` Euals (0)
- ``ne`` Not equal
- ``cs / hs`` carry set / unsigned higher
- ``,`` TODO VEDERE QUALE MANCA
- --
- ``mi`` minus / negative
- ``pl`` plus / $\ge0$
- ``vs`` overflow
- ``vc`` no overflow
- ``hi`` unsigned higher
- ``ls`` unsigned $\le$
- ``ge`` signed $\ge$
- ``lt`` signed <
- ``gt`` signed >
- ``le`` signed $\le$
- ``al`` always 

### Suffix s
Aggiorna il registo ``cprs`` per valutare condizioni su quell'espressione

> Esempio ``ADDneS r1, r1, #3``  
> Esegue l'operazione $\iff$ l'espressione precedente ha dato risultato $\ge 0$ e aggiorna il registro di stato, così le successive consizioni verranno valutate sui risultati di questa espressione

### Istruzioni di confronto
- ``cmp r0, r1`` esegue una sottrazione senza memorizzare il risultato
- ``cmn r0, r1`` come ``cmp r0, -r1``
- ``tst r0 r1`` Fa un'and tra i registri
- ``teq r0 r1`` Xor tra i registri

## Chiamate di funzioni
``bl label``  
_Salta alla label e memorizza in r14 (**lr**) l'indirizzo dell'istruzione successiva_  

Per tornare alla funzione chiamante, ``mov pc, lr``

### Convenzioni
- Registri r0-r3 modificabili dalle funzioni
- Registri r4-r14 non modificabili dalle funzioni

Se le funzioni/la funzione chiamante vogliono usare gli altri registri, li salvano in memoria prima e li ripristinano dopo
- Store di registri multipli ``stfmd sp!, {r4-r5}`` 
- Load ``ldmfd sp!, {r4-r5}``