.data
n: .word -17
m: .word -2
o: .word -3
p: .word -4

  .text
  ldr r5, =n
  ldr r0, [r5]
  ldr r5, =m
  ldr r5, [r5]
  add r0, r5
  ldr r5, =o
  ldr r5, [r5]
  add r0, r5
  ldr r5, =p
  ldr r5, [r5]
  add r0, r5

  mov r1, r0
  asr r1, #0x2

  mov r2, #0x1
  add r2, r2, lsl #0x1
  ldr r5, =n
  ldr r5, [r5]
  mul r2, r5

  ldr r5, =n
  ldr r3, [r5]
  sub r3, asr #0x4

  ldr r4, =n
  ldr r4, [r4]
  ror r4, #0x1f
  and r4, #0x1


  .end
