# Processori

**Circuiti integrati**  
_Unità contenenti circuiti logici_

## Silicio
- **Tansistor**  
_Silicio drogato: inseriti atomi esterni_
- Collegamenti tra i componenti
- Isolamenti elettrici
- **Fotolitografia**  
_Alcune parti coperte da materiale fotosensibile e altre no, ottenendo una parte solidificata_

### Wafer

![Wafer silicio](imgs/wafer.jpg "Wafer di silicio")

Chip diventano più piccoli:
- \+ Efficienza 
- \- Dissipazione del calore
- \- Difficoltà collegamenti

# Chip di memoria (RAM)

Integrati con molti registri, raccolti in locazioni (generalmente di 8 bit)  
  
Accesso ai registri:
1. Si seleziona la locazione dal suo indirizzo in memoria
2. Si definisce la modalità d'accesso (r/w)

Contenuto segnale I/O:
- Indirizzo
- Dati (Input e Output)
- Controllo
    - R/W
    - Output Enable
    - Chip select (Quale banco di RAM)


![Memory Input](imgs/ram_input.png "Input chip di memoria")  
![Memory Output](imgs/ram_out.jpg "Output chip di memoria")  
![Memory Implementation](imgs/ram_implementation.png "Implementazione chip di memoria")

Motivazione **buffer** anziché AND:  
Se arriva corrente dalla parte opposta, una porta AND si brucerebbe, il buffer la dissipa

**Tipi di RAM:**
- SRAM (Static):  
Quella delle immagini sopra, utilizza i latch per memorizzare i bit.  
SOno veloci e costose, vengono utilizzate per la cache
- DRAM (Dyncamic):  
Lente e capienti, utilizzate nei banchi di RAM del PC

## **DRAM** (Ram dinamiche)

Composte da:
- 1 Condensatore, il cui stato (carica/scarica) determina il valore del bit (1/0)
- 1 Transistor, che controlla l'accesso al condensatore (controllo: **word line**)

VS Static:
- \+ Più economiche e compatte
- \- Più lente
- \- Più complesso il controllo  
Serve un circuito di **refresh** della carica ogni ciclo di clock: 1ms per la scarica del condensatore

![Implementazione DRAM](imgs/dram_implementation.jpg "Implementazione DRAM")

Spiegazione componenti:
a0 e a1: bit di selezione della riga  
Ottengo i dati di tutti i bit in quella riga  
RAS serve per dire se sono in lettura o in scrittura  
L'output dei bit viene amplificato e mandato a un latch per memorizzarlo
Poi con un mux selezioni la colonna

Accesso alla memoria:
1. Contenuto riga in un registro
2. Letti i bit selezionati della riga
3. Si usa il registro per le locazioni consecutive

## **DDR SDRAM** (Double Data Rate Synchronous DRAM)

**Sync:**  
_Trasmissione sincrona, con clock, di un pacchetto (locazioni consecutive) ogni ciclo (ma molti cicli per il primo pacchetto)_

**DDR:**
_Due pacchetti ogni ciclo di clock_

### Elementi DRAM

**Banda passante:**  
_Quantità di dati consecutivi leggibili in un tempo t_

**Tempo di accesso:**  
_Tempo necessario per un'operazione in memoria_

_Le nuove DRAM migliorano più la banda passante che il tempo d'accesso_

Si può decidere la dimensione di una locazione

$$`Capacità = 2 ^{\text{linee indirizzo}} \times \text{Linee dato}`$$

### Moduli di memoria

Schede di memoria:
- Circuito stampato con la DRAM
- Memoria distribuita su più chip
- In appositi slot sulla mobo
- Diverse connesisoni:
    - DIMM, double inline memory module
    - SODIMM, small outline DIMM

# Memorie permanenti
Necessarie per:
- Calcolatori embedded non programmabili
- Calcolatori embedded a sostituzione disco magnetico (smartphone, tablet)
- Calcolatori per memorizzare bios, os, sw...

## Tipi di memorie permanenti
- ROM
- PROM (Scrivibili una volta, bit: fusibili)
- EPROM (Cancellabili con esposizione a raggi ultravioletti, bit: carica elettrica)
- EEPROM (Cancellabili elettricamente al singolo bit)
- Flash (Eeprom cancellabili a banchi)