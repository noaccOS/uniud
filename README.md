## UniUD Notes
Git containing notes from university

**Warning:** all future markdown will contain only $`\LaTeX`$ rendered by Code. Do not attempt to read them from the gitlab render.

## Objective
Have a nice and ordered study location

## Markdown formatting
- Argument definition: h1
- Subcategories: h2
- And so on...
- Logic implication: ->
- Logic consequence: =>
- Tables: Tabbed and formatted in source
- Comments and Examples: Blockquotes
- List: List (un/ordered)
- Text formatting:
	- Bold   -> asterisks
	- Italic -> underscore
- Definition: 
	1. Bold element
	2. New line
	3. Italic definition

> Example:  
> **Element**  
> _This is this element's definition_

> If no general argument is defined, start with h2

## TODO

- [x] Rewrite math documents in pure $`\LaTeX`$
- [x] Standardize md usage
- [x] Insert images in architecture circuits
- [ ] Assign real titles to some math documents
- [ ] Learn Org mode
- [ ] Decide if it's better to use $`\LaTeX`$ or Org to PDF
