(TeX-add-style-hook
 "2019-10-01.exercises"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "inputenc"
    "amsfonts"
    "amssymb"
    "amsmath"))
 :latex)

