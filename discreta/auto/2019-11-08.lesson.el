(TeX-add-style-hook
 "2019-11-08.lesson"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "inputenc"
    "amsfonts"
    "amssymb"
    "mathtools"))
 :latex)

